## Settings ##
1. Virtual Env setings
2. Django 2.2 installation
3. Project Creation
4. VS code Settings

## Things to learn ##
- Docker isntallation
- Nginx installation
- pipenv
- gunicorn

## Theme installation ##
- Bootstrap 4 theme
- Django Theme


## Functionalities ##
-Login/Register App Creation
- Login Page
- Registration Page
- Reset Password
- Form validation
- Authenticaton and Authorization
- Permission for users
    - Read
    - write
    - Delete
- List of Users
- Email Sending
- Forgot password