# Check Requirements in your system #
- Go to Django Dir
- pip freeze
- copy to the requirement.txt file using following command
- pip freeze > requirements.txt



## virtual env ##
workon dj-py3-venv
pip install -r requirements.txt

## Install project using Pipenv and Docker ##
- https://www.codingforentrepreneurs.com/blog/django-on-docker-a-simple-introduction

pip install pipenv

## Run server using Gunicone##
-- cd project_folder
- gunicorn project_name.wsgi:application --bind 0.0.0.0:8000

## install project ##

## pipenv install django==2.2.4 gunicorn --python 3.6

####
The problems that Pipenv seeks to solve are multi-faceted:

- You no longer need to use pip and virtualenv separately. They work together.
- Managing a requirements.txt file can be problematic, so Pipenv uses the upcoming Pipfile and Pipfile.lock instead, which is superior for basic use cases.
- Hashes are used everywhere, always. Security. Automatically expose security vulnerabilities.
- Give you insight into your dependency graph (e.g. $ pipenv graph).
- Streamline development workflow by loading .env files.


### Kill Port ###
1. pip install -r requirement.txt
2. sudo kill -9 $(sudo lsof -t -i:8000)
3. python -m django --version
4. Create Project: django-admin startproject project_name
5. Start project: python manage.py runserver (ip:8080)
6. Create App: python manage.py startapp app_name
7. New Table to db:python manage.py makemigrations app_name
8. Check the table: python manage.py sqlmigrate app_name 0001
9. python manage.py migrate
10. python manage.py shell
11. Admin access: python manage.py createsuperuser

## Development CheckList ##
- https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/
